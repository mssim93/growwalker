//
//  QuestList.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Require Attributes :
//  @NSManaged var no: NSNumber?
//  @NSManaged var body: String?
//  @NSManaged var title: String?
//  @NSManaged var prev: NSNumber?
//  @NSManaged var rewardGold: NSNumber?
//  @NSManaged var rewardItems: String?
//  @NSManaged var rewardEnergy: NSNumber?
//  @NSManaged var clear: NSNumber?
//

import Foundation


class QuestList {
    let formatter = NSNumberFormatter()
    var questDict = [Int: Array<String>]()
    var dictionary = [String: String]()
    var itemArray:[[String: String]] = [
        ["body":"","title":"","prev":"0","gold":"","items":"","energy":""],
        ["body":"식물을 심어보세요","title":"심기(1)","prev":"0","gold":"1000","items":"","energy":""],
        ["body":"식물을 수확하세요","title":"수확(1)","prev":"0","gold":"1000","items":"","energy":""]
    ]
    func getDictionary() -> NSDictionary {
        var dict: [Int: NSDictionary] = [:]
        for no in 0 ..< itemArray.count{
            let noString = formatter.stringFromNumber(no)
            itemArray[no]["no"] = noString
            dict[no] = NSDictionary.init(dictionary: itemArray[no])
        }
        return NSDictionary.init(dictionary: dict)
    }
    func getArray() -> [[String: String]] {
        return itemArray
    }
    
}