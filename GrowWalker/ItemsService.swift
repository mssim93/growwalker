//
//  File.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class ItemsService {
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    let formatter = NSNumberFormatter()
    
    func initData(){
        let data = getAll()
        if data.count < 1 {
            let list = ItemsList()
            let array = list.getArray()
            for no in 0 ..< array.count {
                let dictionary = array[no]
                let cost = formatter.numberFromString(dictionary["cost"]!)!
                create(no, name: dictionary["name"]!, desc: dictionary["desc"]!, cost: cost)
            }
        }
    }
    
    func create(no: NSNumber, name: String, desc:String, cost:NSNumber) -> Items {
        
        let newItem = NSEntityDescription.insertNewObjectForEntityForName(Items.entityName, inManagedObjectContext: context) as! Items
        
        
        newItem.no = no
        newItem.name = name
        newItem.desc = desc
        newItem.cost = cost
        
        return newItem
    }

    
    func get(no:NSNumber) -> Items {
        
        let predicate = NSPredicate.init(format: "no = %d", no)
        let fetchRequest = NSFetchRequest(entityName: Items.entityName)
        
        fetchRequest.predicate = predicate
        // var index = 0;
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            if(response.count > 1){
                return response.first as! Items
            } else {
                return Items()
            }
        } catch let error as NSError {
            // failure
            print(error)
            return Items()
        }
        
    }
    // Gets a Status by id
    func getById(id: NSManagedObjectID) -> Items? {
        return context.objectWithID(id) as? Items
    }
    
//    // Updates a Status
//    func update(updatedStatus: Items){
//        if let Items = getById(updatedStatus.objectID){
//            Items.collected = updatedStatus.collected
//        }
//    }
    
    func getAll() -> [Items]{
        return getWith(withPredicate: NSPredicate(value:true))
    }
    
    func getWith(withPredicate queryPredicate: NSPredicate) -> [Items]{
        let fetchRequest = NSFetchRequest(entityName: Items.entityName)
        
        fetchRequest.predicate = queryPredicate
        
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            return response as! [Items]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [Items]()
        }
    }
}
