//
//  InventoryService.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class InventoryService {
    
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    // Saves all changes
    func saveChanges(){
        do{
            try context.save()
        } catch let error as NSError {
            // failure
            print(error)
        }
    }
    
    func create(position: NSNumber, item: NSNumber, amount: NSNumber) -> Inventory {
        
        let newItem = NSEntityDescription.insertNewObjectForEntityForName(Inventory.entityName, inManagedObjectContext: context) as! Inventory
        
        newItem.position = position
        newItem.item = item
        newItem.amount = amount
        
        self.saveChanges()
        return newItem
    }
    
    // Gets a Status by id
    func getById(id: NSManagedObjectID) -> Inventory? {
        return context.objectWithID(id) as? Inventory
    }
    
    
    func get(position:NSNumber) -> Inventory {
        
        let predicate = NSPredicate.init(format: "position = %d", position)
        let fetchRequest = NSFetchRequest(entityName: Inventory.entityName)
        
        fetchRequest.predicate = predicate
        // var index = 0;
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            if(response.count < 1){
                return create(position, item: 0, amount: 0)
            } else {
                return response.last as! Inventory
            }
        } catch let error as NSError {
            // failure
            print(error)
            return Inventory()
        }
    }
    func getAll() -> [Inventory]{
        return getWith(withPredicate: NSPredicate(value:true))
    }
    
    func getWith(withPredicate queryPredicate: NSPredicate) -> [Inventory]{
        let fetchRequest = NSFetchRequest(entityName: Inventory.entityName)
        
        fetchRequest.predicate = queryPredicate
        
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            return response as! [Inventory]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [Inventory]()
        }
    }
    
    // Updates a Status
    func update(updatedStatus: Inventory){
        if let Inventory = getById(updatedStatus.objectID){
            Inventory.position = updatedStatus.position
            Inventory.item = updatedStatus.item
            Inventory.amount = updatedStatus.amount
        }
    }
    
    // Deletes a Status
    func deleteStatus(id: NSManagedObjectID){
        if let statusToDelete = getById(id){
            context.deleteObject(statusToDelete)
        }
    }
}