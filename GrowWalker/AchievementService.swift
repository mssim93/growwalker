//
//  File.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class AchievementService {
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    let formatter = NSNumberFormatter()
    
    func initData(){
        let data = getAll()
        if data.count < 1 {
            let list = AchievementList()
            let array = list.getArray()
            for no in 0 ..< array.count {
                var achieved:NSNumber = 0
                if no == 0 {
                    achieved = 2
                }
                let dictionary = array[no]
                create(no, title: dictionary["title"]!, body: dictionary["body"]!, prev: formatter.numberFromString(dictionary["prev"]!)!, gold: formatter.numberFromString(dictionary["gold"]!)!, items: dictionary["items"]!, energy: formatter.numberFromString(dictionary["energy"]!)!, achieved: achieved)
            }
        }
    }
    
    func create(no: NSNumber, title: String, body:String, prev:NSNumber, gold:NSNumber, items:String, energy:NSNumber, achieved:NSNumber) -> Achievement {
        
        let newItem = NSEntityDescription.insertNewObjectForEntityForName(Achievement.entityName, inManagedObjectContext: context) as! Achievement
        
        
        newItem.no = no
        newItem.title = title
        newItem.body = body
        newItem.prev = prev
        newItem.rewardGold = gold
        newItem.rewardItems = items
        newItem.rewardEnergy = energy
        newItem.achieved = achieved
        
        return newItem
    }
    
    
    func get(no:NSNumber) -> Achievement {
        
        let predicate = NSPredicate.init(format: "no = %d", no)
        let fetchRequest = NSFetchRequest(entityName: Achievement.entityName)
        
        fetchRequest.predicate = predicate
        // var index = 0;
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            if(response.count < 1){
                return Achievement()
            } else {
                return response.last as! Achievement
            }
        } catch let error as NSError {
            // failure
            print(error)
            return Achievement()
        }
        
    }
    // Gets a Status by id
    func getById(id: NSManagedObjectID) -> Achievement? {
        return context.objectWithID(id) as? Achievement
    }
    
    // Updates a Status
    func update(updatedStatus: Achievement){
        if let Achievement = getById(updatedStatus.objectID){
            Achievement.achieved = updatedStatus.achieved
        }
    }
    
    func getAll() -> [Achievement]{
        return getWith(withPredicate: NSPredicate(value:true))
    }
    
    func getWith(withPredicate queryPredicate: NSPredicate) -> [Achievement]{
        let fetchRequest = NSFetchRequest(entityName: Achievement.entityName)
        
        fetchRequest.predicate = queryPredicate
        
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            return response as! [Achievement]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [Achievement]()
        }
    }
}
