//
//  StatusService.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class StatusService {
    
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    let generalService = GeneralService()
    let groundService = GroundService()
    let plantService = PlantsService()
    let itemsService = ItemsService()
    let inventoryService = InventoryService()
    let questService = QuestService()
    let achievementService = AchievementService()
    
    
    // Saves all changes
    func saveChanges(){
        do{
            try context.save()
        } catch let error as NSError {
            // failure
            print(error)
        }
    }

    func initData(){
        itemsService.initData()
        questService.initData()
        plantService.initData()
        achievementService.initData()
    }
    
    func getGeneralStatus() -> General{
        return generalService.get()
    }
    
    func getGroundStatus(position:NSNumber) -> Ground{
        return groundService.get(getGeneralStatus().ground!,position: position)
    }
    func getPlantStatus(no:NSNumber) -> Plants{
        return plantService.get(no)
    }
    
    func addValue(value1:NSNumber, value2:NSNumber) -> NSNumber {
        
        let result = value1.longLongValue + value2.longLongValue
        
        return NSNumber.init(longLong: result)
        
    }
    
    func updateGold(gold:NSNumber){
        let general = getGeneralStatus()
        
        general.gold = addValue(general.gold!, value2: gold)
        
        generalService.update(general)
        self.saveChanges()
    }
    
    func updateEnergy(energy:NSNumber){
        let general = getGeneralStatus()
        
        general.energy = addValue(general.energy!, value2: energy)
        general.latestUpdate = NSDate()
        
        generalService.update(general)
        self.saveChanges()
    }
    
    func updateGround(ground:NSNumber){
        let general = getGeneralStatus()
        
        general.ground = ground
        
        generalService.update(general)
        self.saveChanges()
    }
    //물탱크 채우기
    func updateWater(water:NSNumber){
        let general = getGeneralStatus()
        
        general.water = addValue(general.water!, value2: water)
        generalService.update(general)
        self.saveChanges()
    }
    //땅에 물주기
    func updateMoisture(moisture:NSNumber){
        let general = getGeneralStatus()
        
        general.moisture = addValue(general.moisture!, value2: moisture)
        generalService.update(general)
        self.saveChanges()
    }
    
    //식물 수집
    func collectPlants(no:NSNumber){
        let plant = getPlantStatus(no)
        
        plant.collected = true
        
        plantService.update(plant)
        self.saveChanges()
    }
    
    //식물 심기
    func plantPlants(position:NSNumber){
        let ground = getGroundStatus(position)
        
        ground.hasGrown = true
        ground.startDate = NSDate()
        
        groundService.update(ground)
        self.saveChanges()
    }
    //식물 수확
    func reapPlants(position:NSNumber){
        let ground = getGroundStatus(position)
        
        ground.hasGrown = false
        ground.nutrients = false
        
        groundService.update(ground)
        self.saveChanges()
    }
    //영양제 적용
    func applyNutrients(position:NSNumber){
        let ground = getGroundStatus(position)
        
        ground.nutrients = true
        
        groundService.update(ground)
        self.saveChanges()
    }
    
    //인벤토리에 추가
    func setInventory(position:NSNumber, item:NSNumber, amount:NSNumber){
        let inventory = inventoryService.get(position)
        
        inventory.item = item
        inventory.amount = amount
        
        inventoryService.update(inventory)
        self.saveChanges()
    }
    
    //인벤토리 아이템 갯수 변경
    func updateInventory(position:NSNumber, amount:NSNumber){
        let inventory = inventoryService.get(position)
        
        inventory.amount = amount
        
        inventoryService.update(inventory)
        self.saveChanges()
    }
    
    //모든 퀘스트 목록
    func getAllQuestList() -> [Quest]{
        return questService.getAll()
    }
    
    //가능한 퀘스트 목록
    func getQuestList() -> [Quest]{
        let predicate = NSPredicate.init(format: "clear = %d", 1)
        return questService.getWith(withPredicate: predicate)
    }
    
    //클리어한 퀘스트 목록
    func getClearedQuestList() -> [Quest] {
        let predicate = NSPredicate.init(format: "clear = %d", 2)
        return questService.getWith(withPredicate: predicate)
    }
    
    //퀘스트 수취
    func acceptQuest(no:NSNumber){
        let quest = questService.get(no)
        
        quest.clear = 1 //0: Non, 1: Accept, 2: Clear
        
        questService.update(quest)
        self.saveChanges()
    }
    
    //퀘스트 클리어
    func clearQuest(no:NSNumber){
        let quest = questService.get(no)
        
        quest.clear = 2 //0: Non, 1: Accept, 2: Clear
        
        questService.update(quest)
        self.saveChanges()
        
        let predicate = NSPredicate.init(format: "prev = %d", no)
        let nexts:[Quest] = questService.getWith(withPredicate: predicate)
        
        for next in nexts {
            acceptQuest(next.no!)
        }
    }
    
    //모든 업적 목록
    func getAllAchievementList() -> [Achievement]{
        return achievementService.getAll()
    }
    
    //가능한 업적 목록
    func getQuestList() -> [Achievement]{
        let predicate = NSPredicate.init(format: "achieved = %d", 1)
        return achievementService.getWith(withPredicate: predicate)
    }
    
    //달성한 업적 목록
    func getClearedQuestList() -> [Achievement] {
        let predicate = NSPredicate.init(format: "achieved = %d", 2)
        return achievementService.getWith(withPredicate: predicate)
    }
    
    //업적 활성화
    func acceptAchievement(no:NSNumber){
        let achievement = achievementService.get(no)
        
        achievement.achieved = 1 //0: Non, 1: Active, 2: Achieved
        
        achievementService.update(achievement)
        self.saveChanges()
    }
    
    //업적 달성
    func achieveAchievement(no:NSNumber){
        let achievement = achievementService.get(no)
        
        achievement.achieved = 1 //0: Non, 1: Active, 2: Achieved
        
        achievementService.update(achievement)
        self.saveChanges()
        self.saveChanges()
        
        let predicate = NSPredicate.init(format: "prev = %d", no)
        let nexts:[Achievement] = achievementService.getWith(withPredicate: predicate)
        
        for next in nexts {
            acceptAchievement(next.no!)
        }
    }
}