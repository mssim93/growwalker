//
//  File.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class PlantsService {
    let context = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    let formatter = NSNumberFormatter()
    
    func initData(){
        let data = getAll()
        if data.count < 1 {
            let list = PlantsList()
            let array = list.getArray()
            for no in 0 ..< array.count {
                let dictionary = array[no]
                create(no, name: dictionary["name"]!, desc: dictionary["desc"]!, rare: dictionary["rare"]!)
            }
        }
    }
    
    func create(no: NSNumber, name: String, desc:String, rare:String) -> Plants {
        
        let newItem = NSEntityDescription.insertNewObjectForEntityForName(Plants.entityName, inManagedObjectContext: context) as! Plants
        
        
        newItem.no = no
        newItem.name = name
        newItem.desc = desc
        newItem.rare = rare
        newItem.collected = false
        
        return newItem
    }
    
    func get(no:NSNumber) -> Plants {
        
        let predicate = NSPredicate.init(format: "no = %d", no)
        let fetchRequest = NSFetchRequest(entityName: Plants.entityName)
        
        fetchRequest.predicate = predicate
       // var index = 0;
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            if(response.count > 1){
                return response.last as! Plants
            } else {
                return Plants()
            }
        } catch let error as NSError {
            // failure
            print(error)
            return Plants()
        }
        
    }
    // Gets a Status by id
    func getById(id: NSManagedObjectID) -> Plants? {
        return context.objectWithID(id) as? Plants
    }
    
    // Updates a Status
    func update(updatedStatus: Plants){
        if let Plants = getById(updatedStatus.objectID){
            Plants.collected = updatedStatus.collected
        }
    }
    
    func getAll() -> [Plants]{
        return getWith(withPredicate: NSPredicate(value:true))
    }
    
    func getWith(withPredicate queryPredicate: NSPredicate) -> [Plants]{
        let fetchRequest = NSFetchRequest(entityName: Plants.entityName)
        
        fetchRequest.predicate = queryPredicate
        
        do {
            let response = try context.executeFetchRequest(fetchRequest)
            return response as! [Plants]
            
        } catch let error as NSError {
            // failure
            print(error)
            return [Plants]()
        }
    }
}
