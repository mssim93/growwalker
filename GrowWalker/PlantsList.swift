//
//  PlantsList.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Required Attributes
//  @NSManaged var no: NSNumber?
//  @NSManaged var name: String?
//  @NSManaged var desc: String?
//  @NSManaged var rare: String?
//  @NSManaged var collected: NSNumber?
//

import Foundation


class PlantsList {
    
    var PlantsDict = [Int: Array<String>]()
    var itemArray:[[String: String]] = [
        ["name":"민들레", "desc":"길가에서 흔하게 볼 수 있는 꽃. 특이하게 생긴 씨앗을 가지고 있어 아이들이 장난치는데 쓰기도 한다.","rare":"N"]
    ]
    
    func getArray() -> [[String: String]] {
        return itemArray
    }
    
}