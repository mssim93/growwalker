//
//  General+CoreDataProperties.swift
//  GrowWalker
//
//  Created by 이준수 on 2016. 7. 1..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension General {

    @NSManaged var gold: NSNumber?          //골드
    @NSManaged var energy: NSNumber?        //에너지
    @NSManaged var latestUpdate: NSDate?    //마지막 에너지 업데이트 날짜시간
    @NSManaged var ground: NSNumber?        //심을 수 있는 공간수
    @NSManaged var water: NSNumber?         //물탱크에 차있는 물의 양
    @NSManaged var moisture: NSNumber?      //땅의 수분양
    
}
