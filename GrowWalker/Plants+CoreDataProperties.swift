//
//  Plants+CoreDataProperties.swift
//  GrowWalker
//
//  Created by 이재득 on 2016. 7. 4..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Plants {

    @NSManaged var no: NSNumber?
    @NSManaged var name: String?
    @NSManaged var desc: String?
    @NSManaged var rare: String?
    @NSManaged var collected: NSNumber?

}
