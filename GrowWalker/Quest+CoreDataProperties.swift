//
//  Quest+CoreDataProperties.swift
//  GrowWalker
//
//  Created by 이재득 on 2016. 7. 6..
//  Copyright © 2016년 Team6. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Quest {

    @NSManaged var no: NSNumber?
    @NSManaged var body: String?
    @NSManaged var title: String?
    @NSManaged var prev: NSNumber?
    @NSManaged var rewardGold: NSNumber?
    @NSManaged var rewardItems: String?
    @NSManaged var rewardEnergy: NSNumber?
    @NSManaged var clear: NSNumber?

}
